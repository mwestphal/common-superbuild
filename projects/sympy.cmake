superbuild_add_project_python(sympy
  PACKAGE
    sympy
  DEPENDS
    pythonsetuptools
    pythonmpmath
  LICENSE_FILES
    LICENSE
  SPDX_LICENSE_IDENTIFIER
    BSD-3-Clause
  SPDX_COPYRIGHT_TEXT
    "Copyright (c) 2006-2023 SymPy Development Team"
    "Copyright (c) 2006-2018 SymPy Development Team, 2013-2023 Sergey B Kirpichev"
    "Copyright (c) 2014 Matthew Rocklin"
    "Copyright (c) 2009-2023, PyDy Authors"
    "Copyright 2016, latex2sympy"
  )
